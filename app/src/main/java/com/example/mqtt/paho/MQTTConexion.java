package com.example.mqtt.paho;

import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mqtt.data.MQTTDatos;
import com.example.mqtt.data.MQTTMessageCallback;
import com.example.mqtt.data.server;
import com.google.android.material.snackbar.Snackbar;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.HashMap;
import java.util.Objects;

public class MQTTConexion {

    private final static String TAG = "MQTTConexion";
    public static MQTTConexion Instance;

    MqttAndroidClient mqttAndroidClient;

    HashMap<String, MQTTMessageCallback> callbacks;

    String serverUri = String.format("tcp://%s:%s", server.serverIP, server.puerto);
    String clientId = "AndroidClient";
    AppCompatActivity activity;

    public MQTTConexion(AppCompatActivity activity) {
        this.activity = activity;
        Instance = this;
        callbacks = new HashMap<>();
    }


    /**
     * Clase de ayuda para construir dinamicamente las rutas.
     */
    public static class MQTTRoutes {
        private static String main = "tp2";

        // RASPBERRY PI ZERO
        public static String tempCPU() {
            return main + "/raspi/cpu_thermal";
        }

        // NODEMCU
        public static String temperatura() {
            return main + "/tmp175";
        }

        // KL46
        private static String acelerometro() {
            return main + "/acelerometro";
        }

        public static String acelerometroX() {
            return acelerometro() + "/x";
        }

        public static String acelerometroY() {
            return acelerometro() + "/y";
        }

        public static String acelerometroZ() {
            return acelerometro() + "/z";
        }

        public static String sensorLuz() {
            return main + "/sensorLuz";
        }

        private static String pulsador() {
            return main + "/pulsador";
        }

        public static String pulsador1() {
            return pulsador() + "/p1";
        }

        public static String pulsador2() {
            return pulsador() + "/p2";
        }

        private static String led() {
            return main + "/led";
        }

        public static String ledRojo() {
            return led() + "/rojo";
        }

        public static String ledVerde() {
            return led() + "/verde";
        }

        static String[] subscriptionPaths = {sensorLuz(), tempCPU(), temperatura(), acelerometroX(), acelerometroY(), acelerometroZ(), pulsador1(), pulsador2()};
        static String[] publishPaths = {ledRojo(), ledVerde()};
    }

    /**
     * Suscribe a todos los topicos que están en la clase MQTTRoutes.
     */
    private void subscribeToAllTopics() {
        for (String path :
                MQTTRoutes.subscriptionPaths) {
            subscribeToTopic(path);
        }
    }

    public void setMessageCallback(String topic, MQTTMessageCallback callback) {
        callbacks.put(topic, callback);
    }

    public void deleteMessageCallback(String topic) {
        callbacks.remove(topic);
    }

    public void conexion() {
        clientId = clientId + System.currentTimeMillis();

        mqttAndroidClient = new MqttAndroidClient(activity.getApplicationContext(), serverUri, clientId);
        mqttAndroidClient.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {

                if (reconnect) {
                    addToHistory("Reconnected to : " + serverURI);
                    // Because Clean Session is true, we need to re-subscribe
                    subscribeToAllTopics();
                } else {
                    addToHistory("Connected to: " + serverURI);
                }
            }

            @Override
            public void connectionLost(Throwable cause) {
                addToHistory("The Connection was lost.");
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                //addToHistory("Incoming message: " + new String(message.getPayload()));
                //handleMessage(topic, message);
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });

        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setAutomaticReconnect(true);
        mqttConnectOptions.setCleanSession(false);
        try {
            //addToHistory("Connecting to " + serverUri);
            mqttAndroidClient.connect(mqttConnectOptions, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    DisconnectedBufferOptions disconnectedBufferOptions = new DisconnectedBufferOptions();
                    disconnectedBufferOptions.setBufferEnabled(true);
                    disconnectedBufferOptions.setBufferSize(100);
                    disconnectedBufferOptions.setPersistBuffer(false);
                    disconnectedBufferOptions.setDeleteOldestMessages(false);
                    mqttAndroidClient.setBufferOpts(disconnectedBufferOptions);
                    subscribeToAllTopics();
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    addToHistory("Failed to connect to: " + serverUri);
                    Log.e(TAG, "onFailure: " + exception.getMessage());
                }
            });


        } catch (MqttException ex) {
            ex.printStackTrace();
        }
    }

    private void handleMessage(String topic, MqttMessage message) {
        MQTTDatos.Instance.datos.put(topic, new String(message.getPayload()));
        if (callbacks.containsKey(topic)) {
            callbacks.get(topic).OnMessageReceived(new String(message.getPayload()));
        }
    }

    private void addToHistory(String mainText) {
        System.out.println("LOG: " + mainText);
        Snackbar.make(activity.findViewById(android.R.id.content), mainText, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();

    }

    public void publishMessage(String topic, String message) throws MqttException {
        mqttAndroidClient.publish(topic,new MqttMessage(message.getBytes()));
    }
    public void subscribeToTopic(String topic){
        try {
            mqttAndroidClient.subscribe(topic, 0, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    addToHistory("Subscribed to topic: " + topic);
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    addToHistory("Failed to subscribe to topic: " + topic);
                }
            });

            mqttAndroidClient.subscribe(topic, 0, new IMqttMessageListener() {
                @Override
                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    // message Arrived!
                    //System.out.println("Message: " + topic + " : " + new String(message.getPayload()));
                    handleMessage(topic, message);
                }
            });

        } catch (MqttException ex){
            System.err.println("Exception whilst subscribing");
            ex.printStackTrace();
        }
    }

}
