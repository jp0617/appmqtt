package com.example.mqtt.data;

public interface MQTTMessageCallback
{
    public void OnMessageReceived(String message);
}
