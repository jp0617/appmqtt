package com.example.mqtt.ui.NodeMCU;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class NodeMCUViewModel extends ViewModel {

    private final MutableLiveData<String> mText;

    public NodeMCUViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is slideshow fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}