package com.example.mqtt.ui.RpiZero;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class RpiZeroViewModel extends ViewModel {

    private final MutableLiveData<String> mText;

    public RpiZeroViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is gallery fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}