package com.example.mqtt.ui.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.mqtt.data.server;
import com.example.mqtt.databinding.FragmentSettingsBinding;
import com.google.android.material.snackbar.Snackbar;

public class SettingsFragment extends Fragment {

    private FragmentSettingsBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        SettingsViewModel settingsViewModel =
                new ViewModelProvider(this).get(SettingsViewModel.class);

        binding = FragmentSettingsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        Button btn  = binding.button;

        EditText servidor = binding.servidor;
        EditText puerto = binding.puerto;

        // Obtengo las preferencias guardadas (si existen) y las encajo en el edit text.
        SharedPreferences prefs = getActivity().getSharedPreferences("MQTTIp", Context.MODE_PRIVATE);
        String savedIP = prefs.getString("serverIP","servidor");
        String savedPort = prefs.getString("puerto","puerto");
        servidor.setText(savedIP);
        puerto.setText(savedPort);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                server.serverIP =servidor.getText().toString();
                server.puerto=puerto.getText().toString();

                SharedPreferences prefs = getActivity().getSharedPreferences("MQTTIp", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor =  prefs.edit();
                editor.putString("serverIP",server.serverIP);
                editor.putString("puerto",server.puerto);
                editor.apply();

                Snackbar.make(view,"Opciones guardadas",Snackbar.LENGTH_LONG).show();
            }
        });

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}