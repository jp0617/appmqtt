package com.example.mqtt.ui.NodeMCU;

import static android.content.ContentValues.TAG;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.anychart.APIlib;
import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.SingleValueDataSet;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.charts.CircularGauge;
import com.anychart.core.axes.Circular;
import com.anychart.core.cartesian.series.Column;
import com.anychart.core.gauge.pointers.Bar;
import com.anychart.enums.Anchor;
import com.anychart.enums.HoverMode;
import com.anychart.enums.Position;
import com.anychart.enums.TooltipPositionMode;
import com.anychart.graphics.vector.Fill;
import com.anychart.graphics.vector.SolidFill;
import com.anychart.graphics.vector.text.HAlign;
import com.anychart.graphics.vector.text.VAlign;
import com.example.mqtt.data.MQTTDatos;
import com.example.mqtt.databinding.FragmentNodeMcuBinding;
import com.example.mqtt.paho.MQTTConexion;

import java.util.ArrayList;
import java.util.List;

public class NodeMCUFragment extends Fragment {

    private FragmentNodeMcuBinding binding;
    CircularGauge circularGauge;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        NodeMCUViewModel nodeMCUViewModel =
                new ViewModelProvider(this).get(NodeMCUViewModel.class);

        binding = FragmentNodeMcuBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        MQTTConexion.Instance.setMessageCallback(MQTTConexion.MQTTRoutes.temperatura(), message -> {
            Log.d(TAG, "OnMessageReceived: " + message);
            updateChart(message);
        });

        initializeChart();
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @SuppressLint("DefaultLocale")
    private void updateChart(String message) {
        APIlib.getInstance().setActiveAnyChartView(binding.anyChartView);
        Float data = Float.parseFloat(message);
        circularGauge.data(new SingleValueDataSet(new Float[] {data, 100.0f}));
        circularGauge.label(0d)
                .text(String.format("%.2f °C",data))
                .useHtml(true)
                .padding(0,0,0,-20d)
                .hAlign(HAlign.CENTER)
                .vAlign(VAlign.MIDDLE);
    }

    /**
     * Inicializa el gráfico Gauge.
     */
    private void initializeChart() {
        AnyChartView anyChartView = binding.anyChartView;
        anyChartView.setProgressBar(binding.progressBar);

        circularGauge = AnyChart.circular();
        float dato = 42f;

        // Si hay dato guardado en cache obtenerlo de ahí
        if(MQTTDatos.Instance.datos.containsKey(MQTTConexion.MQTTRoutes.temperatura())){
            dato = Float.parseFloat(MQTTDatos.Instance.datos.get(MQTTConexion.MQTTRoutes.temperatura()));
        }

        circularGauge.data(new SingleValueDataSet(new Float[]{dato,100f}));

        circularGauge.fill("#fff")
                .stroke(null)
                .padding(0d, 0d, 0d, 0d)
                .margin(100d, 100d, 100d, 100d);

        circularGauge.startAngle(-90d);
        circularGauge.sweepAngle(180d);
        circularGauge.animation(true);

        Circular xAxis = circularGauge.axis(0)
                .radius(100d)
                .width(1d)
                .fill((Fill) null);

        // Valor minimo y máximo
        xAxis.scale()
                .minimum(0d)
                .maximum(100d);

        // Deshabilitar labels en medio de la barra
        xAxis.labels().enabled(false);
        xAxis.ticks().enabled(false);
        xAxis.minorTicks().enabled(false);

        // Barra de datos
        Bar bar0 = circularGauge.bar(0d);
        bar0.dataIndex(0d);
        bar0.radius(100d);
        bar0.width(17d);
        bar0.fill(new SolidFill("#64b5f6", 1d));
        bar0.stroke(null);
        bar0.zIndex(5d);

        // Barra de fondo gris
        Bar bar100 = circularGauge.bar(100d);
        bar100.dataIndex(1d);
        bar100.radius(100d);
        bar100.width(17d);
        bar100.fill(new SolidFill("#F5F4F4", 1d));
        bar100.stroke("1 #e5e4e4");
        bar100.zIndex(4d);

        circularGauge.label(0d)
                .text(String.format("%.2f °C",dato))
                .useHtml(true)
                .padding(0,0,0,-20d)
                .hAlign(HAlign.CENTER)
                .vAlign(VAlign.MIDDLE);

        circularGauge.label(1d).enabled(false);

        circularGauge.margin(50d, 50d, 50d, 50d);
        circularGauge.title()
                .text("Temperatura CPU")
                .useHtml(true);
        circularGauge.title().enabled(true);
        circularGauge.title().hAlign(HAlign.CENTER);
        circularGauge.title()
                .padding(0d, 0d, 0d, 0d)
                .margin(0d, 0d, 20d, 0d);

        anyChartView.setChart(circularGauge);
    }
}