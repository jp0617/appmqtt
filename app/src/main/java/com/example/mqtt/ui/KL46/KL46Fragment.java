package com.example.mqtt.ui.KL46;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.core.cartesian.series.Column;
import com.anychart.enums.Anchor;
import com.anychart.enums.HoverMode;
import com.anychart.enums.Position;
import com.anychart.enums.TooltipPositionMode;
import com.example.mqtt.data.MQTTDatos;
import com.example.mqtt.databinding.FragmentKl46Binding;
import com.example.mqtt.paho.MQTTConexion;

import org.eclipse.paho.client.mqttv3.MqttException;

import java.util.ArrayList;
import java.util.List;

public class KL46Fragment extends Fragment {
    private static final String TAG = "KL46Fragment";
    private FragmentKl46Binding binding;

    private Cartesian cartesian;
    private List<DataEntry> datachart;
    private Float[] datos = new Float[4];
    private TextView p1;
    private TextView p2;
    private Switch ledrojo;
    private Switch ledverde;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        KL46ViewModel KL46ViewModel =
                new ViewModelProvider(this).get(KL46ViewModel.class);

        binding = FragmentKl46Binding.inflate(inflater, container, false);
        View root = binding.getRoot();
        instancias();
        return root;
    }

    private void instancias(){
        p1 = binding.valuepulsador1;
        p2 = binding.valuepulsador2;
        ledverde = binding.ledverde;
        ledrojo = binding.ledrojo;
        cartesian = AnyChart.column();
        datachart = new ArrayList<>();
        datos[0]=1.8f;
        datos[1]=2f;
        datos[2]=3f;
        datos[3]=4f;
        p1.setText("1");
        p2.setText("2");
    }

    @Override
    public void onStart() {
        super.onStart();
        initializeChart();
        callback();
        onCheckedChanged();
    }

    private void initializeChart() {
        AnyChartView anyChartView = binding.anyChartView;
        anyChartView.setProgressBar(binding.progressBar);
        cargadatos();
        datachart.add(new ValueDataEntry("Sensor Luz", datos[0]));
        datachart.add(new ValueDataEntry("Acelerometro x", datos[1]));
        datachart.add(new ValueDataEntry("Acelerometro y", datos[2]));
        datachart.add(new ValueDataEntry("Acelerometro z", datos[3]));

        Column column = cartesian.column(datachart);

        column.tooltip()
                .titleFormat("{%X}")
                .position(Position.CENTER_BOTTOM)
                .anchor(Anchor.CENTER_BOTTOM)
                .offsetX(0d)
                .offsetY(5d)
                .format("${%Value}{groupsSeparator: }");

        cartesian.animation(true);

        cartesian.yScale().minimum(0d);

        cartesian.yAxis(0).labels().format("${%Value}{groupsSeparator: }");

        cartesian.tooltip().positionMode(TooltipPositionMode.POINT);
        cartesian.interactivity().hoverMode(HoverMode.BY_X);

        cartesian.xAxis(0).title("Product");
        cartesian.yAxis(0).title("Revenue");

        anyChartView.setChart(cartesian);
        callback();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @SuppressLint("DefaultLocale")
    private void updateChart(String message,String topico) {
        switch (topico){
            case "luz":
                datachart.get(0).setValue("Senson Luz",Float.parseFloat(message));
                break;
            case "x":
                datachart.get(1).setValue("Acelerometro x",Float.parseFloat(message));
                break;
            case "y":
                datachart.get(2).setValue("Acelerometro y",Float.parseFloat(message));
                break;
            case "z":
                datachart.get(3).setValue("Acelerometro z",Float.parseFloat(message));
                break;
            default:
                break;
        }
    }

    private void cargadatos(){
        if (MQTTDatos.Instance.datos.containsKey(MQTTConexion.MQTTRoutes.pulsador1())){
            p1.setText(MQTTDatos.Instance.datos.get(MQTTConexion.MQTTRoutes.pulsador1()));
        }
        if (MQTTDatos.Instance.datos.containsKey(MQTTConexion.MQTTRoutes.pulsador1())){
            p2.setText(MQTTDatos.Instance.datos.get(MQTTConexion.MQTTRoutes.pulsador1()));
        }
        if(MQTTDatos.Instance.datos.containsKey(MQTTConexion.MQTTRoutes.sensorLuz())){
            datos[0] = Float.parseFloat(MQTTDatos.Instance.datos.get(MQTTConexion.MQTTRoutes.sensorLuz()));
        }
        if(MQTTDatos.Instance.datos.containsKey(MQTTConexion.MQTTRoutes.acelerometroX())){
            datos[1] = Float.parseFloat(MQTTDatos.Instance.datos.get(MQTTConexion.MQTTRoutes.acelerometroX()));
        }
        if(MQTTDatos.Instance.datos.containsKey(MQTTConexion.MQTTRoutes.acelerometroY())){
            datos[2] = Float.parseFloat(MQTTDatos.Instance.datos.get(MQTTConexion.MQTTRoutes.acelerometroY()));
        }
        if(MQTTDatos.Instance.datos.containsKey(MQTTConexion.MQTTRoutes.acelerometroZ())){
            datos[3] = Float.parseFloat(MQTTDatos.Instance.datos.get(MQTTConexion.MQTTRoutes.acelerometroZ()));
        }
    }

    private void callback(){
        MQTTConexion.Instance.setMessageCallback(MQTTConexion.MQTTRoutes.sensorLuz(), message -> {
            Log.d(TAG, "OnMessageReceived: " + message);
            updateChart(message,"luz");
        });
        MQTTConexion.Instance.setMessageCallback(MQTTConexion.MQTTRoutes.acelerometroX(), message -> {
            Log.d(TAG, "OnMessageReceived: " + message);
            updateChart(message,"x");
        });
        MQTTConexion.Instance.setMessageCallback(MQTTConexion.MQTTRoutes.acelerometroY(), message -> {
            Log.d(TAG, "OnMessageReceived: " + message);
            updateChart(message,"y");
        });
        MQTTConexion.Instance.setMessageCallback(MQTTConexion.MQTTRoutes.acelerometroZ(), message -> {
            Log.d(TAG, "OnMessageReceived: " + message);
            updateChart(message,"z");
        });
    }

    public void onCheckedChanged(){
        ledrojo.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b){
                try {
                    MQTTConexion.Instance.publishMessage(MQTTConexion.MQTTRoutes.ledRojo(),"1");
                } catch (MqttException e) {
                    Log.e(TAG, "Error mandando MQTT: " + e.getMessage() );
                }
            }else{
                try {
                    MQTTConexion.Instance.publishMessage(MQTTConexion.MQTTRoutes.ledRojo(),"0");
                } catch (MqttException e) {
                    Log.e(TAG, "Error mandando MQTT: " + e.getMessage() );
                }
            }
        });

        ledverde.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b){
                try {
                    MQTTConexion.Instance.publishMessage(MQTTConexion.MQTTRoutes.ledVerde(),"1");
                } catch (MqttException e) {
                    Log.e(TAG, "Error mandando MQTT: " + e.getMessage() );
                }
            }else{
                try {
                    MQTTConexion.Instance.publishMessage(MQTTConexion.MQTTRoutes.ledVerde(),"0");
                } catch (MqttException e) {
                    Log.e(TAG, "Error mandando MQTT: " + e.getMessage() );
                }
            }
        });
    }

}